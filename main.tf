variable "vsphere_user"     { type = "string" }
variable "vsphere_password" { type = "string" }
variable "vsphere_server"   { type = "string" }

provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

module "vmware" {
  source = "git::https://gitlab.com/qbrd-homelab/terraform/modules.git//vmware-data"
}

data "vsphere_virtual_machine" "template" {
  name          = "pfSense"
  datacenter_id = module.vmware.datacenter.id
}

resource "vsphere_virtual_machine" "vm" {
  name             = "pfsense01"
  resource_pool_id = module.vmware.pool.id
  datastore_id     = module.vmware.datastore.id

  num_cpus = 2
  memory   = 4096
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  network_interface {
    network_id   = module.vmware.network.id
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
    label            = "disk0"
    size             = "${data.vsphere_virtual_machine.template.disks.0.size}"
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"
  }
}

output "template" { value = data.vsphere_virtual_machine.template }
